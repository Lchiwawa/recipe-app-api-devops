terraform {
  backend "s3" {
    bucket         = "lc-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
  # profile = "Liwang_IAM"
  shared_credentials_file = "/.aws/credentials"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    ManagedBy   = "Terraform"
    Owner       = var.contact
    Project     = var.project
  }
}